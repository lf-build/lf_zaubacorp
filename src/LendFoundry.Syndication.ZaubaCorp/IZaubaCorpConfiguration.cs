﻿namespace LendFoundry.Syndication.ZaubaCorp
{
    public interface IZaubaCorpConfiguration
    {
        string BaseUrl { get; set; }
    }
}
