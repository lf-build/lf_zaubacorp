﻿namespace LendFoundry.Syndication.ZaubaCorp
{
    public interface IZaubaCorpProxy
    {
        string GetHttpResponseContent(string webUrl);
    }
}
