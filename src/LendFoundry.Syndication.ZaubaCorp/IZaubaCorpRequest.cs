﻿namespace LendFoundry.Syndication.ZaubaCorp
{
    public interface IZaubaCorpRequest
    {
        string CinNumber { get; set; }
    }
}
