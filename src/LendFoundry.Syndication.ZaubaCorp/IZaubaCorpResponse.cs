﻿using System;

namespace LendFoundry.Syndication.ZaubaCorp
{
    public interface IZaubaCorpResponse
    {
        string NameOfCompany { get; set; }
        string TypeOfCompany { get; set; }
        int DirectorNetworkCount { get; set; }
        DateTime DateOfIncorporation { get; set; }
        DateTime DateOfFilingLastBalanceSheet { get; set; }
        decimal AuthorizedCapital { get; set; }
        decimal PaidUpCapital { get; set; }
        double TypeOfCompanyScore { get; set; }
        double DirectorNetworkCountScore { get; set; }
        double DateOfIncorporationScore { get; set; }
        double DateOfFilingLastBalanceSheetScore { get; set; }
        double AuthorizedCapitalScore { get; set; }
        double PaidUpCapitalScore { get; set; }
        double OverallScore { get; set; }
    }
}
