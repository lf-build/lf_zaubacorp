﻿namespace LendFoundry.Syndication.ZaubaCorp
{
    public interface IZaubaCorpService
    {
        IZaubaCorpResponse GetZaubaCorpData(IZaubaCorpRequest request);
    }
}
