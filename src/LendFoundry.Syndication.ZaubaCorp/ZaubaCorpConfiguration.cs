﻿namespace LendFoundry.Syndication.ZaubaCorp
{
    public class ZaubaCorpConfiguration : IZaubaCorpConfiguration
    {
        public string BaseUrl { get; set; }
    }
}
