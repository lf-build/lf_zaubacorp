﻿using System;

namespace LendFoundry.Syndication.ZaubaCorp
{
    public class ZaubaCorpProxy : IZaubaCorpProxy
    {

        public string GetHttpResponseContent(string webUrl)
        {
            var http = new RestSharp.Http();
            http.Url = new Uri(webUrl);
            var response = http.Get();
            return response.Content;
        }

    }
}
