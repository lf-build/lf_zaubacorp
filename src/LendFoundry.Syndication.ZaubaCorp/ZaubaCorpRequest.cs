﻿namespace LendFoundry.Syndication.ZaubaCorp
{
    public class ZaubaCorpRequest : IZaubaCorpRequest
    {
        public string CinNumber { get; set; }
    }
}
