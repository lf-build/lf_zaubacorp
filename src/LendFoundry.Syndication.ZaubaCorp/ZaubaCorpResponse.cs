﻿using System;

namespace LendFoundry.Syndication.ZaubaCorp
{
    public class ZaubaCorpResponse : IZaubaCorpResponse
    {
        public string NameOfCompany { get; set; }
        public string TypeOfCompany { get; set; }
        public int DirectorNetworkCount { get; set; }
        public DateTime DateOfIncorporation { get; set; }
        public DateTime DateOfFilingLastBalanceSheet { get; set; }
        public decimal AuthorizedCapital { get; set; }
        public decimal PaidUpCapital { get; set; }
        public double TypeOfCompanyScore { get; set; }
        public double DirectorNetworkCountScore { get; set; }
        public double DateOfIncorporationScore { get; set; }
        public double DateOfFilingLastBalanceSheetScore { get; set; }
        public double AuthorizedCapitalScore { get; set; }
        public double PaidUpCapitalScore { get; set; }
        public double OverallScore { get; set; }
    }
}
