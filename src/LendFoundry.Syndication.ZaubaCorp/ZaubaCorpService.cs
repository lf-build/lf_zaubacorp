﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Threading;

namespace LendFoundry.Syndication.ZaubaCorp
{
    public class ZaubaCorpService : IZaubaCorpService
    {
        public ZaubaCorpService(IZaubaCorpConfiguration configuration,
            IZaubaCorpProxy proxy)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (proxy == null)
                throw new ArgumentNullException(nameof(proxy));

            Configuration = configuration;
            Proxy = proxy;

            _lockObject = new object();
        }

        private IZaubaCorpConfiguration Configuration { get; }
        private IZaubaCorpProxy Proxy { get; }

        private object _lockObject;
        public IZaubaCorpResponse GetZaubaCorpData(IZaubaCorpRequest request)
        {
            var finalUrl = Configuration.BaseUrl + "//" + request.CinNumber;
            var returnValue = new ZaubaCorpResponse();

            HtmlAgilityPack.HtmlDocument rootCompanyDocument = new HtmlAgilityPack.HtmlDocument();
            rootCompanyDocument.LoadHtml(Proxy.GetHttpResponseContent(finalUrl));

            var intermediateTable = new Dictionary<string, string>();
            var intermediateTableNodes = rootCompanyDocument.DocumentNode.SelectNodes("//table[@class='table table-striped']");
            foreach (var tableNode in intermediateTableNodes)
            {
                var trNodes = tableNode.Descendants("tr").ToList();
                foreach (var trNode in trNodes)
                {
                    var tdNodes = trNode.Descendants("td").ToList();
                    if (tdNodes.Count > 1 && intermediateTable.ContainsKey(tdNodes[0].InnerText) == false)
                    {
                        intermediateTable.Add(tdNodes[0].InnerText, tdNodes[1].InnerText);
                    }
                }
            }

            returnValue.NameOfCompany = string.Empty;
            if (intermediateTable.ContainsKey("Class of Company"))
            {
                returnValue.TypeOfCompany = intermediateTable["Class of Company"];
            }

            var directorInOtherCompanies = new List<string>();
            var directorNetworkRootNodes = rootCompanyDocument.DocumentNode.SelectNodes("//div[@class='col-lg-12 col-md-12 col-sm-12 col-xs-12']");
            foreach (var directorRootNode in directorNetworkRootNodes)
            {
                var anchorTags = directorRootNode.Descendants("a").ToList();
                Parallel.ForEach(anchorTags, (anchor) =>
                {
                    if (anchor.Attributes["href"] != null && anchor.Attributes["href"].Value.ToLower().Contains("/director/"))
                    {
                        var directorSubDocument = new HtmlAgilityPack.HtmlDocument();
                        directorSubDocument.LoadHtml(Proxy.GetHttpResponseContent(anchor.Attributes["href"].Value));

                        var directorNetwork = directorSubDocument.DocumentNode.SelectNodes("//table[@class='table table-striped']");
                        if (directorNetwork != null)
                        {
                            foreach (var directorDetailNode in directorNetwork)
                            {
                                var detailAnchors = directorDetailNode.Descendants("a").ToList();
                                foreach (var detailAnchor in detailAnchors)
                                {
                                    if (detailAnchor.Attributes["href"] != null && detailAnchor.Attributes["href"].Value.ToLower().Contains("https://www.zaubacorp.com/company/"))
                                    {
                                        if (detailAnchor.Attributes["href"].Value.ToLower().Contains(returnValue.NameOfCompany.ToLower()) == false)
                                        {
                                            lock (_lockObject)
                                            {
                                                directorInOtherCompanies.Add(detailAnchor.Attributes["href"].Value);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            }

            returnValue.DateOfIncorporation = DateTime.MinValue;
            returnValue.DateOfFilingLastBalanceSheet = DateTime.MinValue;
            returnValue.AuthorizedCapital = -1.00M;
            returnValue.PaidUpCapital = -1.00M;
            returnValue.TypeOfCompany = string.Empty;

            if(intermediateTable.ContainsKey("Company Name"))
            {
                returnValue.NameOfCompany = intermediateTable["Company Name"];
            }
            if (intermediateTable.ContainsKey("Date of Incorporation"))
            {
                try
                {
                    returnValue.DateOfIncorporation = DateTime.ParseExact(intermediateTable["Date of Incorporation"], "dd MMMM yyyy", new CultureInfo("en-us"));
                }
                catch
                {

                }
            }
            if (intermediateTable.ContainsKey("Date of Latest Balance Sheet"))
            {
                try
                {
                    returnValue.DateOfFilingLastBalanceSheet = DateTime.ParseExact(intermediateTable["Date of Latest Balance Sheet"], "dd MMMM yyyy", new CultureInfo("en-us"));
                }
                catch
                {

                }
            }
            if (intermediateTable.ContainsKey("Authorised Capital"))
            {
                try
                {
                    returnValue.AuthorizedCapital = decimal.Parse(intermediateTable["Authorised Capital"].Substring(intermediateTable["Authorised Capital"].IndexOf(";") + 1));
                }
                catch
                {

                }
            }
            if (intermediateTable.ContainsKey("Paid up capital"))
            {
                try
                {
                    returnValue.PaidUpCapital = decimal.Parse(intermediateTable["Paid up capital"].Substring(intermediateTable["Paid up capital"].IndexOf(";") + 1));
                }
                catch
                {

                }
            }
          
            returnValue.DirectorNetworkCountScore = 4 * 5.00;
            if (directorInOtherCompanies.Count <= 19)
            {
                returnValue.DirectorNetworkCountScore = 1 * 5.00;
            }
            else if (directorInOtherCompanies.Count <= 39)
            {
                returnValue.DirectorNetworkCountScore = 2 * 5.00;
            }
            else if (directorInOtherCompanies.Count <= 59)
            {
                returnValue.DirectorNetworkCountScore = 3 * 5.00;
            }
            else
            {
                returnValue.DirectorNetworkCountScore = 4 * 5.00;
            }

            returnValue.DateOfIncorporationScore = 4 * 2.50;
            if (returnValue.DateOfIncorporation != DateTime.MinValue)
            {
                int numberOfMonths = (int)((DateTime.Now.Subtract(returnValue.DateOfIncorporation).TotalDays / 365.24) * 12.0);
                if (numberOfMonths <= 23)
                {
                    returnValue.DateOfIncorporationScore = 1 * 2.50;
                }
                else if (numberOfMonths <= 71)
                {
                    returnValue.DateOfIncorporationScore = 2 * 2.50;
                }
                else if (numberOfMonths <= 155)
                {
                    returnValue.DateOfIncorporationScore = 3 * 2.50;
                }
            }

            returnValue.TypeOfCompanyScore = 0.0;
            if (!string.IsNullOrWhiteSpace(returnValue.TypeOfCompany))
            {
                if (returnValue.TypeOfCompany.ToLower().StartsWith("Pu"))
                {
                    returnValue.TypeOfCompanyScore = 4 * 1.25;
                }
                else if (returnValue.TypeOfCompany.ToLower().StartsWith("Pr"))
                {
                    returnValue.TypeOfCompanyScore = 3 * 1.25;
                }
            }

            returnValue.PaidUpCapitalScore = 4 * 1.25;
            if (returnValue.PaidUpCapital > 0.0M)
            {
                if (returnValue.PaidUpCapital <= 100000M)
                {
                    returnValue.PaidUpCapitalScore = 0.0;
                }
                else if (returnValue.PaidUpCapital <= 1000000M)
                {
                    returnValue.PaidUpCapitalScore = 2 * 1.25;
                }
            }

            returnValue.AuthorizedCapitalScore = 4 * 1.25;
            if (returnValue.AuthorizedCapital > 0.0M)
            {
                if (returnValue.AuthorizedCapital <= 100000M)
                {
                    returnValue.AuthorizedCapitalScore = 0.0;
                }
                else if (returnValue.AuthorizedCapital <= 500000M)
                {
                    returnValue.AuthorizedCapitalScore = 2 * 1.25;
                }
                else if (returnValue.AuthorizedCapital <= 2500000M)
                {
                    returnValue.AuthorizedCapitalScore = 3 * 1.25;
                }
            }

            returnValue.DateOfFilingLastBalanceSheetScore = 0.0;
            if (returnValue.DateOfFilingLastBalanceSheet != DateTime.MinValue)
            {
                int numberOfDays = (int)DateTime.Now.Subtract(returnValue.DateOfFilingLastBalanceSheet).TotalDays;
                if (numberOfDays <= 365)
                {
                    returnValue.DateOfFilingLastBalanceSheetScore = 4 * 1.25;
                }
                else if (numberOfDays <= 1095)
                {
                    returnValue.DateOfFilingLastBalanceSheetScore = 3 * 1.25;
                }
                else
                {
                    returnValue.DateOfFilingLastBalanceSheetScore = 2 * 1.25;
                }
            }

            returnValue.OverallScore =
                returnValue.DirectorNetworkCountScore +
                returnValue.TypeOfCompanyScore +
                returnValue.DateOfIncorporationScore +
                returnValue.DateOfFilingLastBalanceSheetScore +
                returnValue.AuthorizedCapitalScore +
                returnValue.PaidUpCapitalScore;
     
            return returnValue;
        }
    }
}
