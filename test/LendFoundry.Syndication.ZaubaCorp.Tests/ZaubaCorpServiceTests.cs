﻿using System;
using Xunit;

namespace LendFoundry.Syndication.ZaubaCorp.Tests
{
    public class ZaubaCorpServiceTests
    {
        public ZaubaCorpServiceTests()
        {
            Configuration = new ZaubaCorpConfiguration() { BaseUrl = "https://www.zaubacorp.com/company" };
        }

        private IZaubaCorpConfiguration Configuration { get; }

        [Fact]
        public void GetLiveData()
        {
            IZaubaCorpService service = new ZaubaCorpService(Configuration, new ZaubaCorpProxy());

            IZaubaCorpRequest tataSteel = new ZaubaCorpRequest() { CinNumber = "L27100MH1907PLC000260" };
            IZaubaCorpRequest infosys = new ZaubaCorpRequest() { CinNumber = "L85110KA1981PLC013115" };
            IZaubaCorpRequest wipro = new ZaubaCorpRequest() { CinNumber = "L32102KA1945PLC020800" };
            IZaubaCorpRequest lufthansa = new ZaubaCorpRequest() { CinNumber = "U63010DL1996PLC076199" };
            IZaubaCorpRequest masterCardIndia = new ZaubaCorpRequest() { CinNumber = "U72300DL2000PTC108269" };
            IZaubaCorpRequest americanExpress = new ZaubaCorpRequest() { CinNumber = "U65921DL1999FLC101368" };
            IZaubaCorpRequest tcsLimited = new ZaubaCorpRequest() { CinNumber = "L22210MH1995PLC084781" };

            DateTime startTime = DateTime.Now;
            var tataSteelResponse = service.GetZaubaCorpData(tataSteel);
            DateTime endTime = DateTime.Now;

            var tataSteelSeconds = endTime.Subtract(startTime).TotalSeconds;

            startTime = DateTime.Now;
            var infosysResponse = service.GetZaubaCorpData(infosys);
            endTime = DateTime.Now;

            var infosysSeconds = endTime.Subtract(startTime).TotalSeconds;

            startTime = DateTime.Now;
            var wiproResponse = service.GetZaubaCorpData(wipro);
            endTime = DateTime.Now;

            var wiproSeconds = endTime.Subtract(startTime).TotalSeconds;

            startTime = DateTime.Now;
            var lufthansaResponse = service.GetZaubaCorpData(lufthansa);
            endTime = DateTime.Now;

            var lufthansaSeconds = endTime.Subtract(startTime).TotalSeconds;

            startTime = DateTime.Now;
            var masterCardIndiaResponse = service.GetZaubaCorpData(masterCardIndia);
            endTime = DateTime.Now;

            var masterCardSeconds = endTime.Subtract(startTime).TotalSeconds;

            startTime = DateTime.Now;
            var americanExpResponse = service.GetZaubaCorpData(americanExpress);
            endTime = DateTime.Now;

            var americanExpSeconds = endTime.Subtract(startTime).TotalSeconds;

            startTime = DateTime.Now;
            var tcsResponse = service.GetZaubaCorpData(tcsLimited);
            endTime = DateTime.Now;

            var tcsSeconds = endTime.Subtract(startTime).TotalSeconds;

        }
    }
}
